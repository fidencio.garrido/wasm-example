use wasm_bindgen::prelude::*;

fn twice(n: isize) -> isize {
    n * 2
}

fn sum(a: isize, b: isize) -> isize {
    a + b
}

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet(name: &str) -> isize {
    alert(&format!("Hello, {}", name));
    return twice(sum(1, 4));
}

#[cfg(test)]
mod tests {
    use crate::sum;

    #[test]
    fn it_works() {
        let result = sum(2, 2);
        assert_eq!(result, 4);
    }
}
