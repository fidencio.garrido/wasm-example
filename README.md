1. Create file, duh
2. Add dep for `wasm-bindgen = "0.2.80"`
3. Add crate-type:
```toml
[lib]
crate-type = ["cdylib"]
```
4. Build with `wasm-pack build` (add `--target web` for ES modules)

## How to export code

```rust
#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet(name: &str) {
    alert(&format!("Hello, {}", name));
}
```